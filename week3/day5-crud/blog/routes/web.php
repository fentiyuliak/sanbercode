<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/master', function () {
//     return view('master');
// });

// Route::get('/', function () {
//     return view('table');
// });
// Route::get('/data-table', function () {
//     return view('data-tables');
// });

Route::get('/', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::get('/cast', 'CastController@store');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::get('/cast/{cast_id}', 'CastController@update');
Route::get('/cast/{cast_id}', 'CastController@destroy');