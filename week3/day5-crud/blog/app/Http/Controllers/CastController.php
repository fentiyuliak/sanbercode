<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CastController extends Controller
{
    //
    public function create()
    {
        return view('create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul_film' => 'required|unique:cast',
            'ringkasan_film' => 'required',
            'tahun_film' => 'required',
        ]);
        $query = DB::table('casts')->insert([
            "judul_film" => $request["judul_film"],
            "ringkasan_film" => $request["ringkasan_film"],
            "tahun_film" => $request["tahun_film"],
        ]);
        return redirect('/cast');
    }

    public function index()
    {
        $casts = DB::table('casts')->get();
        return view('index', compact('casts'));
    }

    public function show($id)
    {
        $casts = DB::table('casts')->where('id', $id)->first();
        return view('show', compact('casts'));
    }

    public function edit($id)
    {
        $casts = DB::table('casts')->where('id', $id)->first();
        return view('edit', compact('casts'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'judul_film' => 'required|unique:cast',
            'ringkasan_film' => 'required',
            'tahun_film' => 'required',
        ]);
        $query = DB::table('casts')->insert([
            "judul_film" => $request["judul_film"],
            "ringkasan_film" => $request["ringkasan_film"],
            "tahun_film" => $request["tahun_film"],
        ]);
        return redirect('/cast');
    }

    public function destroy($id)
    {
        $query = DB::table('casts')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
