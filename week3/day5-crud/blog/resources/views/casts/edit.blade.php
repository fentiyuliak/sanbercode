@extends('master')

@section('title')
    Add Cast
@endsection
@section('content')
<div>
    <h2>Edit Post {{$post->id}}</h2>
    <form action="/casts/{{$post->id}}" method="POST">
        @csrf
        @method('PUT')


        <div class="form-group">
                <label for="title">judul</label>
                <input type="text" class="form-control" name="judul" id="title" placeholder="Masukkan Judul Film">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
        </div>


        <div class="form-group">
            <label for="body">ringkasan</label>
            <input type="text" class="form-control" name="ringkasan" id="body" placeholder="Masukkan Ringkasan Film">
            @error('body')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">tahun</label>
            <input type="text" class="form-control" name="tahun" id="body" placeholder="Masukkan Tahun Film">
            @error('body')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection