  
@extends('master')

@section('title')
    Cast
@endsection
@section('content')
    <a href="/casts/create" class="btn btn-primary">Tambah</a>
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Judul_film</th>
                <th scope="col">Ringkasan_film</th>
                <th scope="col">tahun_film</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($casts as $key=>$value)
                <tr>
                    <td>{{ $key + 1 }}</th>
                    <td>{{ $value->Judul_film }}</td>
                    <td>{{ $value->Ringkasan_film }}</td>
                    <td>{{ $value->tahun_film }}</td>
                    <td>
                        <a href="/casts/{{ $value->id }}" class="btn btn-info">Show</a>
                        <a href="/casts/{{ $value->id }}/edit" class="btn btn-primary">Edit</a>
                        <form action="/casts/{{ $value->id }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection